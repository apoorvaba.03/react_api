import React, { useState, useEffect} from 'react';
import ReactDom from 'react';
function Display(props){
    const [data,setData] = useState();

    useEffect(() => {
        fetch(`https://api.github.com/users/ShashikumarKL`)
        .then((response) => response.json())
        .then(setData);
    },[]);
    console.log(data);
    if(data)
    {
        return(
            <>
            <h1>{data.login}</h1>,
            <img src={data.avatar_url} alt="shhs"/>
            
            </>
        );
    }
    return <div>no data availabe</div>

}
export default Display;