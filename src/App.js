import logo from './logo.svg';
import './App.css';
import React from 'react';
import ReactDOM from 'react';
//import React, { useEffect, useState } from 'react';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event)
  {
    this.setState({value:event.target.value});
  }
  handleSubmit(event){
     alert('A name was '+this.state.value);
    event.preventDefault();
  }
  render(){
  return (
    <form onSubmit={this.handleSubmit}>
      <label>
        name:<input type="text" value={this.state.value}  onChange={this.handleChange}/>
    </label>
    <input type="submit" value="Submit"/>
    </form>
  );
}
}

export default App;
